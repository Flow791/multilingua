//
//  CoursViewController.swift
//  Multilingua
//
//  Created by Florian Baudin on 24/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class CoursViewController: UIViewController {

    @IBOutlet weak var titreLabel: UILabel!
    @IBOutlet weak var contenuTextView: UITextView!

    var cours:NSDictionary?
    
    @IBAction func passerAExerciceAction(_ sender: Any) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        titreLabel.text = cours!["titre"] as? String
        contenuTextView.text = cours!["contenu"] as? String
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
