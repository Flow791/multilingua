//
//  Questions.swift
//  Multilingua
//
//  Created by Florian Baudin on 20/03/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class Questions {

    let id
    let exerciceID
    let question
    let reponse1
    let reponse2
    let reponse3
    let reponse4
    let exactRep
    
    init(id:Int, exerciceID:Int, question:String, reponse1:String, reponse2:String, reponse3:String, reponse4:String, exactRep:Int) {
        
        self.id = id
        self.exerciceID = exerciceID
        self.question = question
        self.reponse1 = reponse1
        self.reponse2 = reponse2
        self.reponse3 = reponse3
        self.reponse4 = reponse4
        self.exactRep = exactRep
    }
    
    func getId() -> Int {
        return self.id
    }
    
    func getExerciceID() -> String {
        return self.exerciceID
    }
    
    func setExerciceID(exerciceIDu:Sring) {
        self.exerciceID = exerciceID
    }
    
    func getQuestion() -> String {
        return self.question
    }
    
    func setQuestion(question:Sring) {
        self.question = question
    }
    
    func getReponse1() -> String {
        return self.reponse1
    }
    
    func setReponse1(reponse1:Sring) {
        self.reponse1 = reponse1
    }
    
    func getReponse2() -> String {
        return self.reponse2
    }
    
    func setReponse2(reponse2:Sring) {
        self.reponse2 = reponse2
    }
    
    func getReponse3() -> String {
        return self.reponse3
    }
    
    func setReponse3(reponse3:Sring) {
        self.reponse3 = reponse3
    }
    
    func getReponse4() -> String {
        return self.reponse4
    }
    
    func setReponse4(reponse4:Sring) {
        self.reponse4 = reponse4
    }
    
    func getExactRep() -> String {
        return self.exactRep
    }
    
    func setExactRep(exactRep:Sring) {
        self.exactRep = exactRep
    }
    
}
