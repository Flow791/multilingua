//
//  Accueil.swift
//  Multilingua
//
//  Created by Florian Baudin on 21/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit
import UserNotifications

class Accueil: UIViewController, UNUserNotificationCenterDelegate {
    
    let dates:[String] = ["06-03-2017 17:00","08-03-2017 19:00","13-03-2017 17:00","15-03-2017 19:00","17-03-2017 19:00","20-03-2017 18:00",
                          "25-03-2017 17:35"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        removeNotification()
        initNotif()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Couleur d'ecriture de la StatusBar en blanc
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     
     }*/
    
    // MARK: - Notification
    
    func initNotif() {
        
        if #available(iOS 10.0, *) {
            
            
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            let options: UNAuthorizationOptions = [.alert,.sound,.badge]
            
            center.requestAuthorization(options: options) { (granted,error) in
                if !granted {
                    print("Notifications refusées")
                }
            }
            
            center.getNotificationSettings { (settings) in
                if settings.authorizationStatus != .authorized {
                    print("Notifications désactivées")
                }
            }
            
            let content = UNMutableNotificationContent()
            content.title = "N'oublie pas la formation"
            content.body = "La prochaine formation à lieu dans une heure"
            content.sound = UNNotificationSound.default()
            
            for index in 0...(dates.count - 1) {
                let date:Date?
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "fr_FR")
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                date = dateFormatter.date(from: dates[index])!
                let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: date!)
                let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
                
                let identifier = "NotificationFormation\(index)"
                let request = UNNotificationRequest(identifier: identifier,
                                                    content: content, trigger: trigger)
                center.add(request, withCompletionHandler: { (error) in
                    if error != nil {
                        print("Something went wrong")
                    }
                })
            }
            
            
        }else {
            print("iOS inferieur 10")
            for index in 0...(dates.count - 1) {
                let notification = UILocalNotification()
                notification.alertTitle = "Hey n'oubliez pas !"
                notification.alertBody = "N'oubliez pas votre formation presentielle dans une heure"
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "fr_FR")
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                let date = dateFormatter.date(from: dates[index])!
                notification.fireDate = date
                
                UIApplication.shared.scheduleLocalNotification(notification)
            }
        }
    }

    
    func removeNotification() {
        
        // loop through the pending notifications
        for notification in UIApplication.shared.scheduledLocalNotifications! as [UILocalNotification] {
            
            // Cancel the notification that corresponds to this task entry instance (matched by taskTypeId)
            UIApplication.shared.cancelLocalNotification(notification)
        }
    }
    

    
    // MARK: - Notification Delegate
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // Play sound and show alert to the user
        completionHandler([.alert,.sound,.badge])
    }
    
    
}
