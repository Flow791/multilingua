//
//  Bouton.swift
//  Multilingua
//
//  Created by Florian Baudin on 21/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class Bouton: UIButton {
    
    //Intialiseur
    
    required init?(coder aDecoder: NSCoder) {
        
        //Initialiseur de la classe parente
        super.init(coder: aDecoder)
        
        //Coins arrondis
        layer.cornerRadius = 27.5
        //Couleur de la bordure
        layer.borderColor = UIColor.init(red: 189.0/255.0, green: 161.0/255.0, blue: 126.0/255.0, alpha: 1.0).cgColor
        //Epaisseur de la bordure
        layer.borderWidth = 1
        //Couleur du texte
        setTitleColor(UIColor.white, for: .normal)
        //Couleur de fond
        backgroundColor = UIColor.init(red: 189.0/255.0, green: 161.0/255.0, blue: 126.0/255.0, alpha: 1.0)
        //Padding a gauche et a droite
        contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        
    }
}
