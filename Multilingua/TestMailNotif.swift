//
//  ViewController.swift
//  Multilingua
//
//  Created by Florian Baudin on 06/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit
import MessageUI
import UserNotifications

class TestMailNotif: UIViewController, MFMailComposeViewControllerDelegate {
    
    var isGrantedNotificationAccess:Bool = false
    
    
    @IBAction func sendMail(_ sender: Any) {
        
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients(["Test@test.fr"])
        mailVC.setSubject("Subject for email")
        mailVC.setMessageBody("Email message string", isHTML: false)
        
        present(mailVC, animated: true, completion: nil)
    }
    
    @IBAction func sendNotif(_ sender: Any) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(
                options: [.alert,.sound,.badge],
                completionHandler: { (granted,error) in
                    self.isGrantedNotificationAccess = granted
            }
            )
            
            if isGrantedNotificationAccess{
                //add notification code here
                let content = UNMutableNotificationContent()
                content.title = "10 Second Notification Demo"
                content.subtitle = "From PocketDev"
                content.body = "Notification after 10 seconds - Your pizza is Ready!!"
                content.categoryIdentifier = "message"
                
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3.0,repeats: false)
                
                let request = UNNotificationRequest(identifier: "10.second.message",content: content,trigger: trigger)
                
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
            
        } else {
            // Fallback on earlier versions
        }
        
        
        
    }

    func mailComposeController(_ controller: MFMailComposeViewController,didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
}
