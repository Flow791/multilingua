//
//  Exercixes.swift
//  Multilingua
//
//  Created by Florian Baudin on 20/03/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class Exercixes {

    let id
    let titre
    let description
    let nbQuestions
    let niveau
    let temps
    
    init(id:Int, titre:Int, desciption:String, nbQuestion:Int, niveau:Int, temps:Int) {
        
        self.id = id
        self.titre = titre
        self.description = description
        self.nbQuestions = nbQuestion
        self.niveau = niveau
        self.temps = temps

    }
    
    func getId() -> Int {
        return self.id
    }
    
    func getTitre() -> String {
        return self.titre
    }
    
    func setTitre(titre:Sring) {
        self.titre = titre
    }
    
    func getDescription() -> String {
        return self.description
    }
    
    func setDescription(description:Sring) {
        self.description = description
    }
    
    func getNbQuestion() -> String {
        return self.nbQuestions
    }
    
    func setNbQuestion(nbQuestion:Sring) {
        self.nbQuestions = nbQuestion
    }
    
    func getNiveau() -> String {
        return self.niveau
    }
    
    func setNiveau(niveau:Sring) {
        self.niveau = niveau
    }
    
    func getTemps() -> String {
        return self.temps
    }
    
    func setTemps(temps:Sring) {
        self.temps = temps
    }
    
}
