//
//  ListeExercicesTableViewController.swift
//  Multilingua
//
//  Created by Florian Baudin on 21/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class ListeExercicesTableViewController: UITableViewController {

    var listeExercices:[[String:AnyObject]]?
    var listeQuestions:[[String:AnyObject]]?
    var timestamp:Double?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initJSONObject(res: "data", ext: "json")
        
        if UserDefaults.standard.double(forKey: "Timestamp") == 0.0 {
            timestamp = NSDate().timeIntervalSince1970
            UserDefaults.standard.register(defaults: ["Timestamp" : timestamp!])
            UserDefaults.standard.set(timestamp, forKey: "Timestamp")
        } else {
            timestamp = UserDefaults.standard.double(forKey: "Timestamp")
        }
    }
    
    func initJSONObject (res: String, ext: String) {
        let url:URL = Bundle.main.url(forResource: res, withExtension: ext )!
        let data = NSData(contentsOf: url)
        
        do {
            let object = try JSONSerialization.jsonObject(with: data as! Data, options: .allowFragments)
            if let dictionary = object as? [String: AnyObject] {
                listeExercices = readJSONObject(object: dictionary)
            }
        } catch {
            print("Erreur de lecture du fichier JSON")
        }
    }
    
    func readJSONObject(object: [String: AnyObject]) -> [[String:AnyObject]]{
        let exercices = object["Exercices"] as? [[String: AnyObject]]
        listeQuestions = object["Questions"] as? [[String: AnyObject]]
        return exercices!
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        let timestampNow = NSDate().timeIntervalSince1970
        let nbADebloquer = (timestampNow - timestamp!)/86400 + 1
        
        if nbADebloquer > 5 {
            return 5
        }
        
        return Int(nbADebloquer)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exoCell", for: indexPath) as! TableViewCell
        cell.titreLabel.text = listeExercices![indexPath.row]["titre"] as! String!
        cell.niveauLabel.text = niveauString(niveau: listeExercices![indexPath.row]["niveau"] as! String)
        cell.tempsLabel.text = tempsString(temps: listeExercices![indexPath.row]["temps"] as! String)
        cell.descriptionTextView.text = listeExercices![indexPath.row]["description"] as! String!

        return cell
    }
    
    func niveauString (niveau: String) -> String {
        
        switch niveau {
        case "1":
            return "Niveau : ⭐"
        case "2":
            return "Niveau : ⭐⭐"
        case "3":
            return "Niveau : ⭐⭐⭐"
        default:
            return "Niveau : ⭐"
        }
    }
    
    func tempsString (temps: String) -> String {
        
        switch temps {
        case "1":
            return "Temps : 🕑"
        case "2":
            return "Temps : 🕑🕑"
        case "3":
            return "Temps : 🕑🕑🕑"
        default:
            return "Temps : 🕑"
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    

     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! ExerciceViewController
        
        if segue.identifier == "voirExercice" {
            let indexPath = tableView.indexPathForSelectedRow
            let ExerciceSelectionne = listeExercices![(indexPath?.row)!] as NSDictionary?
            viewController.exercice = ExerciceSelectionne!
            viewController.listeQuestions = listeQuestions!
        }
    }
}
