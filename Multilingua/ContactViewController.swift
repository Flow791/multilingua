//
//  ContactViewController.swift
//  Multilingua
//
//  Created by Florian Baudin on 22/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate {

    let listeNom:[String] = ["Pierre","Paul","Jacques"]
    let listeMail:NSDictionary = ["Pierre": "flobaud01@gmail.com", "Paul": "flobaud01@icloud.com", "Jacques": "Flow791@outlook.fr"]
    var nom:String?
    
    @IBAction func sendMail(_ sender: Any) {
        
        let mailString = listeMail["\(nom!)"]
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients(["\(mailString!)"])
        mailVC.setSubject("Multilingua")
        mailVC.setMessageBody("", isHTML: false)
        
        present(mailVC, animated: true, completion: nil)
    }
    
    @IBOutlet weak var selectMentor: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        selectMentor.dataSource = self
        selectMentor.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Disparition de la vue et retour à l'accueil apres envoi du mail
    func mailComposeController(_ controller: MFMailComposeViewController,didFinishWith result: MFMailComposeResult, error: Error?) {
        
        let vc : AnyObject! = self.storyboard!.instantiateViewController(withIdentifier: "Accueil")
        self.show(vc as! UIViewController, sender: vc)
       
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - PickerView
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listeMail.count
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        nom = listeNom[row]
        return nom!
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
