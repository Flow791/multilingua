//
//  ExerciceViewController.swift
//  Multilingua
//
//  Created by Florian Baudin on 24/02/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class ExerciceViewController: UIViewController {

    var exercice:NSDictionary?
    var listeQuestions:[[String:AnyObject]]?
    var listeQuestionExercice:[[String:AnyObject]] = []
    var indexQuestion:Int?
    var score:Int?
    
    @IBOutlet weak var titreLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var bouton1Label: UIButton!
    @IBOutlet weak var bouton2Label: UIButton!
    @IBOutlet weak var bouton3Label: UIButton!
    @IBOutlet weak var bouton4Label: UIButton!
    @IBOutlet weak var scoreTextLabel: UILabel!
    @IBOutlet weak var scoreIntLabel: UILabel!
    
    @IBAction func Bouton1Action(_ sender: Any) {
        nextQuestion(idButton: "1")
    }
    @IBAction func bouton2Action(_ sender: Any) {
        nextQuestion(idButton: "2")
    }
    @IBAction func Bouton3Action(_ sender: Any) {
        nextQuestion(idButton: "3")
    }
    @IBAction func Bouton4Action(_ sender: Any) {
        nextQuestion(idButton: "4")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Vidage du tableau de question !")
        listeQuestionExercice.removeAll()
        print("Tableau des questions vide : \(listeQuestionExercice)")
        indexQuestion = 0
        score = 0
        
        titreLabel.text = exercice!["titre"] as? String
        
        for i in 0...(((listeQuestions?.count)!) - 1) {
            
            let idExerciceListeQuestion:String = (listeQuestions![i]["idExercice"]!) as! String
            if idExerciceListeQuestion == (exercice!["id"]!) as! String {
                
                listeQuestionExercice.append(listeQuestions![i])
            }
        }
        updateView()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateView() {
        
        questionLabel.text = listeQuestionExercice[indexQuestion!]["question"] as! String?
        bouton1Label.setTitle(listeQuestionExercice[indexQuestion!]["reponse1"] as! String?, for: .normal)
        bouton2Label.setTitle(listeQuestionExercice[indexQuestion!]["reponse2"] as! String?, for: .normal)
        bouton3Label.setTitle(listeQuestionExercice[indexQuestion!]["reponse3"] as! String?, for: .normal)
        bouton4Label.setTitle(listeQuestionExercice[indexQuestion!]["reponse4"] as! String?, for: .normal)
    }
    
    func nextQuestion(idButton:String) {
        
        if idButton == listeQuestionExercice[indexQuestion!]["reponseCorrecte"] as! String? {
            score! += 1
            print("Score +1")
        }
        
        if indexQuestion! < (listeQuestionExercice.count - 1) {
            
            indexQuestion! += 1
            updateView()
        } else {
            
            let nbQuestion:Int = Int(exercice!["nbQuestion"] as! String)!
            let scoreTot = (score! * 100) / nbQuestion
            scoreIntLabel.text = "\(scoreTot)%"
            
            questionLabel.isHidden = true
            bouton1Label.isHidden = true
            bouton2Label.isHidden = true
            bouton3Label.isHidden = true
            bouton4Label.isHidden = true
            scoreTextLabel.isHidden = false
            scoreIntLabel.isHidden = false
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
