//
//  Cours.swift
//  Multilingua
//
//  Created by Florian Baudin on 20/03/2017.
//  Copyright © 2017 Florian Baudin. All rights reserved.
//

import UIKit

class Cours {
    
    let id
    let titre
    let description
    let typeContenu
    let contenu
    let niveau
    let temps
    let exerciceID
    
    init(id:Int, titre:String, description:String, typeContenu:String, contenu:String, niveau:Int, temps:Int, exerciceID:Int) {
        
        self.id = id
        self.titre = titre
        self.description = description
        self.typeContenu = typeContenu
        self.contenu = contenu
        self.niveau = niveau
        self.temps = temps
        self.exerciceID = exerciceID
    }
    
    func getId() -> Int {
        return self.id
    }
    
    func getTitre() -> String {
        return self.titre
    }
    
    func setTitre(titre:Sring) {
        self.titre = titre
    }
    
    func getDescription() -> String {
        return self.description
    }
    
    func setDescription(description:Sring) {
        self.description = description
    }
    
    func getTypeContenu() -> String {
        return self.typeContenu
    }
    
    func setTypeContenu(typeContenu:Sring) {
        self.typeContenu = typeContenu
    }
    
    func getContenu() -> String {
        return self.contenu
    }
    
    func setContenu(contenu:Sring) {
        self.contenu = contenu
    }
    
    func getNiveau() -> String {
        return self.niveau
    }
    
    func setNiveau(niveau:Sring) {
        self.niveau = niveau
    }
    
    func getTemps() -> String {
        return self.temps
    }
    
    func setTemps(temps:Sring) {
        self.temps = temps
    }
    
    func getExerciceID() -> String {
        return self.exerciceID
    }
    
    func setExerciceID(exerciceIDu:Sring) {
        self.exerciceID = exerciceID
    }

}
